# Cruddery
A small-ish CRUD library for PHP and MySQL.
- [Installation](#installation)
- [Usage](#usage)
- [Methods](#methods)

## Installation
Cruddery can be install via [Composer](https://github.com/composer/composer).
```
$ composer require salby/cruddery
```
This will install cruddery as a dependency in a `vendor/` directory in your project.

## Usage
Use the autoloader in the `vendor/` directory to load the cruddery class.

```php
<?php

// Vendor autoload.
require_once(__DIR__ . '/vendor/autoload.php');

// Initialize the dbee class.
$db = new \salby\dbee\dbee([
    'host' => 'localhost',
    'database' => 'test',
    'user' => 'root',
    'password' => ''
]);

// Initialize the cruddery class.
$crud = new \salby\cruddery\Cruddery($db);

// Do stuff ...
```

## Methods
- [Create](#create)
- [Read](#read)
- [Update](#update)
- [Delete](#delete)

### Create
Coming soon...

### Read
- [Basic usage](#basic-usage)
- [Filter](#filter)
- [Limit and offset](#limit-and-offset)
- [Excluding tables](#excluding-tables)
- [Relations](#relations)
- [Group and order](#order)

#### Basic usage
```php
$users = $crud -> read('user');
```
Returns all rows from the table *user* with joins.

#### Filter
If you want columns to have a certain value, you can set a filter in the config.
```php
$user = $crud -> read('user', [
    'filter' => [
        'last_name' => 'Skywalker'
    ]
]);
```
Returns rows from *user* where **last_name** is 'Skywalker'.

You can also request that a column has one of multiple values by passing an array as the column's value.
```php
$user = $crud -> read('user', [
    'filter' => [
        'last_name' => ['Skywalker', 'Solo']
    ]
]);
```
Returns rows from *user* where **last_name** is 'Skywalker' or 'Solo'.

#### Limit and offset
You can limit the max amount of rows to return by adding limit to the config.
```php
$users = $crud -> read('user', [
    'limit' => 10
]);
```
Returns max. 10 rows from *user*.

You  can also set an offset to skip the first *n* rows.

*Note that you __CAN NOT__ use offset without limit*.
```php
$users = $crud -> read('user', [
    'limit' => 10,
    'offset' => 8
]);
```
Returns max. 10 rows from *user* and skips the first 8 rows.

#### Excluding tables
If you want to keep a foreign table from being joined, use the *exclude* array in the config.
```php
$users = $crud -> read('user', [
    'exclude' => ['email']
]);
```
Returns all rows from the table *user*, but excludes joined columns from the table *email*.

#### Relations
You can disable incoming and outgoing relations by setting them in the config. By default all relations are enabled.
```php
$users = $crud -> read('user', [
    'relations_out' => false,
    'relations_in' => false
]);
```

#### Order
You can use *order_by* in the config to control how the result is ordered.
```php
$users = $crud -> read('user', [
    'order_by' => 'last_name'
]);
```
Returns rows from *user* ordered by **last_name**.

### Update
Coming soon...

### Delete
Coming soon...
