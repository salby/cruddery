<?php

namespace salby\cruddery;

use salby\dblyze\dblyze;
use Exception;

class Delete extends Dblyze
{

    public $baseTable = "";
    public $columns = [];
    public $relationsIn = [];

    protected $db;

    final public function __construct(object $db)
    {
        $this->db = $db;
        parent::__construct($db);
    }

    /**
     * Delete from table.
     *
     * @param string $table
     * @param array $config
     *
     * @return bool
     */
    final public function exec(String $table, array $config = [])
    {

        // Set base-table.
        $this->baseTable = $table;

        // Find primary-keys of targeted rows.
        $primaryKeys = $this->findRows($table, $config);

        // If $primaryKeys is empty, exit.
        if (empty($primaryKeys)) {
            return true;
        }

        // Find all external columns that contain
        // rows that will intefere with the primary
        // delete-operation.
        $external = $this->externalRows($primaryKeys);

        // Iterate over external inteferences
        // and remove them.
        foreach ($external as $table => $target) {
            $this->delete($table, [
                'where' => [
                    [
                        $target => $primaryKeys,
                    ],
                ],
            ]);
        }

        // Execute primary delete-operation
        // on base-table.
        $this->delete($this->baseTable, $config);

        // Returns true if no exceptions
        // are encountered.
        return true;

    }

    /**
     * Removes rows from a table.
     *
     * @param string $table
     * @param array $requirements
     *
     * @return bool
     */
    final public function delete(String $table, array $requirements)
    {

        // Translate $requirements to SQL if
        // they haven't been translated already.
        if (!array_key_exists('sql', $requirements)) {
            $requirements = $this->requirementsToSql($requirements);
        }

        // Assemble SQL query.
        $sql = "DELETE FROM `$table` WHERE " . $requirements['sql'] . ";";

        // Execute query with parameters.
        $this->db->query($sql, $requirements['parameters']);

        return true;

    }

    /**
     * Find relational-tables that contain
     * rows that will intefere with the primary
     * removal of rows.
     * 
     * @param array $primaryKeys - These keys are how the method
     *                             checks if the rows are related.
     *
     * @return array
     */
    final public function externalRows(array $primaryKeys)
    {

        // Array that will hold the rows for
        // each table.
        $return = [];

        // Fetch incoming relations if they
        // haven't been stored already.
        $this->relationsIn = empty($this->relationsIn)
            ? parent::relations([
                'REFERENCED_TABLE_NAME' => $this->baseTable
            ])
            : $this->relationsIn;

        // Iterate over relations and check
        // that the origin-table is a
        // relational table.
        foreach ($this->relationsIn as $rel) {

            $originTable = $rel['TABLE_NAME'];
            $foreignKey = $rel['COLUMN_NAME'];

            // If $originTable is a relational-table,
            // fetch the rows where $foreignKey exists
            // in $primaryKeys.
            if (parent::isRelationalTable($originTable)) {

                /*util::dd([
                    $originTable,
                    $foreignKey,
                ]);*/

                $read = new read($this->db);
                $rows = $read->exec($originTable, [
                    'filter' => [
                        $foreignKey => $primaryKeys,
                    ],
                    'relations_in' => false,
                    'relations_out' => false,
                ]);

                // Store table-key pair if $rows
                // is not empty.
                if (!empty($rows)) {
                    $return[$originTable] = $foreignKey;
                }

            }

        }

        // Return final array of tables that
        // will intefere with the primary
        // delete-operation.
        return $return;

    }

    /**
     * Identify rows based on $requirements and
     * return an array of primary keys.
     *
     * @param string $table
     * @param array $requirements
     *
     * @return array
     */
    final public function findRows(String $table, array $requirements)
    {

        // If $requirements is an array, translate them
        // into valid SQL.
        if (!array_key_exists('sql', $requirements)) {
            $requirements = $this->requirementsToSql($requirements);
        }

        // Fetch columns from the table if they
        // haven't been stored already.
        if (!array_key_exists($table, $this->columns)) {
            $this->columns[$table] = parent::columns($table);
        }
        $columns = $this->columns[$table];

        // Find primary-key column. If there isn't
        // one, throw an exception.
        $primaryKey = parent::findColumn($columns, [
            'find' => [
                'Key' => 'PRI'
            ]
        ]);
        if ($primaryKey === []) {
            throw Exception(
                "The target table \"$table\" has no primary key."
            );
            exit();
        } else {
            $primaryKey = $primaryKey['Field'];
        }

        // Assemble SQL query.
        $sql = "SELECT * FROM `$table` WHERE " . $requirements['sql'] . ";";

        // Fetch rows from table.
        $rows = $this->db->fetch($sql, $requirements['parameters']);

        

        // Create an empty array that will hold the
        // primary keys.
        $primaryKeys = [];

        // Iterate and insert primary keys in $primaryKeys.
        foreach ($rows as $row) {
            $primaryKeys[] = $row[$primaryKey];
        }

        // Return final list of primary keys.
        return $primaryKeys;

    }

    /**
     * Translate requirements into SQL.
     *
     * Returns an array containing a valid SQL string,
     * as well as the parameters needed for the values.
     *
     *      [return]
     *         |
     *         |-`sql`-> "`column` = :placeholder_1 AND ..."
     *         |
     *         |-`parameters`-> [·]
     *                           |
     *                           |-`placeholder_1`-> 'value'
     *                           |
     *                           |-`placeholder_2`-> 1234567
     *                           |
     *                           ·
     *                           ·
     *                           ·
     *
     * @param array $requirements
     *
     * @return array
     */
    final public function requirementsToSql(array $requirements)
    {

        // Start with an empty array that will become
        // the `WHERE` part of a query.
        $sql = [];

        // Create an empty array that will contain the
        // parameters for the SQL.
        $parameters = [];

        if (array_key_exists('where', $requirements)) {
            $translated = $this->translateOptions($requirements['where'], "=");
            $parameters = array_merge($parameters, $translated['parameters']);
            $sql[] = $translated['sql'];
        }

        if (array_key_exists('not', $requirements)) {
            $translated = $this->translateOptions($requirements['not'], "!=");
            $parameters = array_merge($parameters, $translated['parameters']);
            $sql[] = $translated['sql'];
        }

        // Assemble SQL string.
        $sql = implode(' AND ', $sql);

        // Return valid SQL string and parameters.
        return [
            'sql' => $sql,
            'parameters' => $parameters,
        ];

    }

    /**
     * Translate a collection of requirements
     * to SQL.
     *
     * @param array $requirements.
     * @param string $operator.
     *
     * @return array
     */
    final private function translateOptions(array $requirements, String $operator)
    {

        // Array that will hold the values needed
        // when the query is executed.
        $parameters = [];

        // Array that will hold the requirements
        // for later implosion.
        $allReqs = [];

        // Define prefix for the placeholders.
        $prefix = trim($operator) === "!="
            ? "not_"
            : "where_";

        foreach ($requirements as $option) {

            // Temporary collection of requirements.
            $reqs = [];

            foreach ($option as $column => $value) {

                // Use a different operator and multiple
                // placeholders if $value is an array.
                if (is_array($value)) {

                    $tempOperator = trim($operator) === "!="
                        ? "NOT IN"
                        : "IN";
                    $placeholders = [];
                    for ($i = 0; $i < count($value); $i++) {
                        $placeholder = $prefix.$this->baseTable."_".$column."_".$i;
                        $placeholders[] = ":".$placeholder;
                        $parameters[$placeholder] = $value[$i];
                    }
                    $reqs[] = "`$column` $tempOperator (".implode(', ', $placeholders).")";

                } else {

                    $placeholder = $prefix.$this->baseTable."_".$column;
                    $parameters[$placeholder] = $value;
                    $reqs[] = "`$column` $operator :$placeholder";

                }

            }

            // Insert imploded requirements.
            $allReqs[] = implode(' AND ', $reqs);

        }

        // Return a valid SQL string and parameters.
        return [
            'sql' => implode(' OR ', $allReqs),
            'parameters' => $parameters,
        ];

    }

}

