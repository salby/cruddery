<?php

namespace salby\cruddery;

class Cruddery
{

    protected $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    /**
     * # Create
     *
     * ## Config
     * · required
     *
     * @param string $table
     * @param array $columns
     *
     * @return bool
     */
    public function create($table, $rows)
    {
        $create = new create($this->db);
        return $create->exec($table, $rows);
    }

    /**
     * # Read
     *
     * ## Config
     * · required
     *
     * **Array** *filter*
     *
     * **Int** *limit*
     *
     * **Int** *offset*
     *
     * **String** *order_by*
     *
     * **String** *group_by*
     *
     * **Bool** *relations_out*
     *
     * **Bool** *relations_in*
     *
     * **Array** *exclude*
     *
     * **Bool** *merge*
     *
     * @param string $table
     * @param array $config
     *
     * @return array
     */
    public function read($table, $config = [])
    {
        $read = new read($this->db);
        return $read->exec($table, $config);
    }

    /**
     * # Delete
     *
     * Deletes rows from a table.
     *
     * @param string $table
     * @param array $config
     *
     * @return bool
     */
    public function delete($table, $config = [])
    {
        $delete = new delete($this->db);
        return $delete->exec($table, $config);
    }

}
