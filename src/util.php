<?php

namespace salby\cruddery;

class Util
{

    static function relatesOut($table, $column, $relations)
    {

        $relatesOut = false;

        foreach ($relations as $rel) {
            if ($rel['TABLE_NAME'] === $table && $rel['COLUMN_NAME'] === $column
                &&
                $rel['REFERENCED_TABLE_NAME'] !== NULL)
                $relatesOut = true;
        }

        return $relatesOut;

    }

    static function isRelationalTable($columns)
    {

        $primaryKeys = [];

        foreach ($columns as $column) {
            if ($column['Key'] == 'PRI')
                $primaryKeys[] = $column;
        }

        return count($primaryKeys) === 0;

    }

    static function mergeConcat($config)
    {
        $defaults = [
            'separator' => '-----'
        ];
        $config = array_merge($defaults, $config);

        // Explode source string by separator.
        $values = explode($config['separator'], $config['source']);

        // Return array of values.
        return $values;

    }

    /**
     * Sanitize value automatically or by given filter.
     *
     * @param $value
     * @param string $filter
     *
     * @return sanitized $value
     */
    static function sanitize($value, $filter = "")
    {

        // Recursive if $value is an array.
        if (is_array($value)) {
            $sanitized = [];
            foreach ($value as $val) {
                $sanitized[] = self::sanitize($val, $filter);
            }
        } else {

            if ($filter === "string" || is_string($value))
                $sanitized = filter_var($value, FILTER_SANITIZE_STRING);

            if ($filter === 'strip_tags')
                $sanitized = strip_tags($value);

            if ($filter === "email")
                $sanitized = filter_var($value, FILTER_SANITIZE_EMAIL);

            if ($filter === 'encoded')
                $sanitized = filter_var($value, FILTER_SANITIZE_ENCODED);

            if ($filter === 'float' || is_float($value))
                $sanitized = filter_var($value, FILTER_SANITIZE_NUMBER_FLOAT);

            if ($filter === 'int' || is_int($value))
                $sanitized = filter_var($value, FILTER_SANITIZE_NUMBER_INT);

            if ($filter === 'url')
                $sanitized = filter_var($value, FILTER_SANITIZE_URL);

        }

        // Return sanitized value.
        return $sanitized;

    }

    static function function($functions, $function)
    {

        // Split functions into array.
        $functions = explode(';', $functions);

        $found = "";
        foreach ($functions as $fun) {

            // Set found function if it matches parameter.
            if (startsWith(trim($fun), $function))
                $found = trim($fun);

        }

        // Return empty string if no function matching parameter is found.
        if ($found === "")
            return "";

        $match = preg_match('#\((.*?)\)#', $found, $match);
        return $match[1];

    }

    static function startsWith($needle, $haystack)
    {

        return substr($haystack, 0, strlen($needle) - 1) === $needle;

    }

    static function formatSelect($select)
    {

        if (substr_count($select, '`') < 4) {

            // Split table from column and merge with added diacritics.
            $split = explode('.', $select);
            $selectFormatted = "`$split[0]`.`$split[1]`";

        } else {

            $selectFormatted = $select;

        }

        // Return formatted select.
        return $selectFormatted;

    }

    static function removePrefix(array $array, String $prefix)
    {
        $return = [];
        foreach ($array as $key => $value) {
            if (self::startsWith($prefix, $key)) {
                $key = substr($key, strlen($prefix));
            }
            if (is_array($value)) {
                $value = self::removePrefix($value, $prefix);
            }
            $return[$key] = $value;
        }
        return $return;
    }

    static function dd($data)
    {
        highlight_string("<?php\n " . var_export($data, true) . "?>");
        echo '<script>
            document.getElementsByTagName("code")[0].getElementsByTagName("span")[1].remove();
            document.getElementsByTagName("code")[0].getElementsByTagName("span")[document.getElementsByTagName("code")[0].getElementsByTagName("span").length - 1].remove();
        </script>';
        die();
    }

}
