<?php

namespace salby\cruddery;

use salby\dblyze\dblyze;
use Exception;

class Create extends Dblyze
{

    public $baseTable = "";
    public $tables = [];
    public $relationsIn = [];
    public $relationsOut = [];
    public $tempValues = [];

    protected $db;

    final public function __construct($db)
    {
        $this -> db = $db;
        parent::__construct($db);
    }

    final public function exec($baseTable, $rows)
    {

        $this->baseTable = $baseTable;
        $this->tables[$this->baseTable] = [];

        $sortedRows = $this->sortRows($rows);
        
        $queries = [];
        foreach ($sortedRows as $table => $rows) {

            // Prepare and execute query.
            $query = $this->prepareQuery($table, $rows);
            $queries[] = $query;

            // Attempt to execute query.
            $firstRow = $this->db->query($query['sql'], $query['parameters']);

            // Store insert ids temporarily.
            $insertIds = [];
            for ($i = $firstRow; $i < $firstRow + count($rows); $i++) {
                $insertIds[] = (int)$i;
            }
            /**
             * Insert each value in a table-value pair under
             * the correct ID.
             *
             * [tempValues]
             *     |
             *     |--[some_id]
             *     |      |
             *     |      |--[some_table => 8716]
             *     |
             *     |--[other_id]
             *            |
             *            |--[other_table => 1423]
             *
             * Each ID corresponds with a row. A future row
             * in the same sequence might be connected to
             * one we just inserted, and therefore we store
             * the inserted row's id in case the future row
             * needs it in a foreign key.
             */
            for ($i = 0; $i < count($insertIds); $i++) {
                $rowId = array_keys($rows)[$i];
                if (!array_key_exists($rowId, $this->tempValues)) {
                    $this->tempValues[$rowId] = [];
                }
                $this->tempValues[$rowId][$table] = $insertIds[$i];
            }
        
        }

        return true;

    }

    final public function sortRows(array $rows)
    {

        // Fetch base-table columns, they are 100% needed later.
        $this->tables[$this->baseTable] = parent::columns($this->baseTable);

        // Start by finding out what tables are used.
        $sorted = $this->findTables([], $rows);

        // Then fill each table with the rows it matches.
        $sorted = $this->insertRows($sorted, $rows);

        return $sorted;

    }

    /**
     * Returns an SQL query and parameters for values.
     *
     * @param string $table
     * @param array $rows
     *
     * @return array
     */
    final public function prepareQuery(String $table, array $rows)
    {
        
        $parameters = [];

        $columns = [];
        $valueStrings = [];

        // Check if foreign linking is needed.
        $neededColumn = parent::findColumn($this->tables[$table], [
            'find' => [
                'Key' => 'MUL'
            ]
        ]);
        $mul = $neededColumn !== []
            ? ['column' => $neededColumn['Field']]
            : [];

        // Find out what table to look for.
        if (!empty($mul)) {
            $mulRelation = parent::relations([
                'TABLE_NAME' => $table,
                'COLUMN_NAME' => $mul['column'],
            ])[0]['REFERENCED_TABLE_NAME'];
        }

        foreach ($rows as $id => $row) {

            // Find relation value (possibly).
            if (!empty($mul)) {
                $mulValue = $this->tempValues[$id][$mulRelation];
            }

            // Find out which columns to use.    
            foreach ($row as $column => $value) {
                // Insert known columns.
                if (!in_array($column, $columns)) {
                    $columns[] = "`$column`";
                }
                // Insert needed column (possibly).
                if (!empty($mul)) {
                    $columns[] = "`$mul[column]`";
                }
            }

            $placeholders = [];
            $multiRows = false;
            foreach ($row as $column => $value) {
                if (is_array($value)) {

                    // This value is an array of values.
                    $multiRows = true;
                    
                    for ($i = 0; $i < count($value); $i++) {

                        // Create temporary placeholder.
                        $tempPlaceholder = [];

                        // Insert placeholder and parameter for SQL.
                        $tempPlaceholder[] = ":".$id."_".$column."_".$i;
                        $parameters[$id."_".$column."_".$i] = $value[$i];
                        
                        // Insert needed MUL column.
                        if (!empty($mul)) {
                            $tempPlaceholder[] = ":".$id."_".$mul['column']."_".$i;
                            $parameters[$id."_".$mul['column']."_".$i] = $mulValue;
                        }

                        // Insert value-string independently from single-values.
                        $valueStrings[] = "(" . implode(', ', $tempPlaceholder) . ")";

                    }

                } else {

                    // Insert placeholder and parameter for SQL.
                    $placeholders[] = ":" . $id . "_" . $column;
                    $parameters[$id."_".$column] = $value;

                    // Insert needed MUL column.
                    if (!empty($mul)) {
                        $placeholders[] = ":".$id."_".$mul['column'];
                        $parameters[$id."_".$mul['column']] = $mulValue;
                    }
                
                }
            }
            // Insert value-string if value is a single-value.
            if (!$multiRows) {
                $valueStrings[] = "(" . implode(', ', $placeholders) . ")";
            }
            

        }

        // Assemble SQL.
        $sql = "INSERT INTO `$table` (" . implode(', ', array_unique($columns)) . ") VALUES " . implode(', ', $valueStrings) . ";";

        // Return SQL and parameters.
        return [
            'parameters' => $parameters,
            'sql' => $sql
        ];

    }

    /**
     * Insert the rows in their matching tables like this:
     *      
     *      [$list]
     *         |
     *         |-- some_table
     *         |       |
     *         |   *rows belonging to some_table*
     *         |
     *         |-- other_table
     *         |       |
     *         |   *rows belonging to other_table*
     *         ·
     *         ·
     *         ·
     *  Rows are grouped with an unique ID so that they
     *  can be linked later.
     *
     * @param array $list
     * @param array $rows
     *
     * @return array
     */
    final public function insertRows(array $list, array $rows)
    {
        
        foreach ($rows as $row) {

            // Create temporary array to sort the rows values into.
            $tables = [];
            foreach ($list as $table => $rows) {
                if (!array_key_exists($table, $tables)) {
                    $tables[$table] = [];
                }
            }

            // Create ID used to match rows in different tables.
            $id = uniqid();

            foreach ($row as $column => $value) {

                // Table is defined and known.
                if (substr_count($column, '.')) {
                    $table = explode('.', $column)[0];
                    $column = explode('.', $column)[1];
                    $tables[$table][$column] = $value;
                }

                // Column doesn't exist in base table.
                elseif (
                    parent::findColumn($this->tables[$this->baseTable], [
                        'find' => [
                            'Field' => $column
                        ]
                    ]) === []
                ) {
                    if ($this->relatedTableExists($column)) {
                        if (empty($this->tables[$column])) {
                            $this->tables[$column] = parent::columns($column);
                        } 
                        $actualColumn = parent::findColumn($this->tables[$column]);
                        $tables[$column][$actualColumn['Field']] = $value;
                    }
                }


                // Column exists in base table.
                elseif (
                    parent::findColumn($this->tables[$this->baseTable], [
                        'find' => [
                            'Field' => $column
                        ]
                    ]) !== []
                ) {
                    $tables[$this->baseTable][$column] = $value;
                }

            }

            foreach ($tables as $table => $row) {
                $list[$table][$id] = $row;
            }

        }

        return $list; 

    }

    /**
     * Finds all tables neccesary to complete queries.
     *
     * The final list is structured like this:
     *
     *      [$list]
     *         |
     *         |-- some_table => []
     *         |-- other_table => []
     *         |-- third_table => []
     *
     * We can the later insert the rows into their
     * respective tables.
     *
     * @param array $list
     * @param array $rows
     *
     * @return array
     */
    final public function findTables(array $list, array $rows)
    {

        if (!array_key_exists($this->baseTable, $list)) {
            $list[$this->baseTable] = [];
        }

        foreach ($rows as $row) {
            foreach ($row as $column => $value) {

                // Table is defined.
                if (substr_count($column, '.')) {
                    
                    $table = explode('.', $column)[0];
                    if (!array_key_exists($table, $list)) {
                        $list[$table] = [];
                    }
                
                }

                // Table isn't defined, column doesn't exist in base table.
                elseif (
                    parent::findColumn($this->tables[$this->baseTable], [
                        'find' => [
                            'Field' => $column
                        ]
                    ]) === []
                ) {

                    // Check related tables to find table matching column.
                    $foundMatch = $this->relatedTableExists($column);
                    
                    if ($foundMatch) {

                        if (!array_key_exists($column, $list)) {
                            $list[$column] = [];
                        }

                    } else {
                        // Throw exception if you can't find the right table.
                        throw new Exception("
                        The column \"$column\" couldn't find a table. \n
                        try being more specific. \n
                        (e.g. \"some_table.$column\") \n
                        ");
                    }

                }

            }
        }

        return $list;

    }

    /**
     * Check if table is related to base table.
     *
     * @param string $tableName
     *
     * @return bool
     */
    final public function relatedTableExists(String $tableName)
    {

        // Get related tables if it hasn't been done already.
        if (empty($this->relationsIn)) {
            $this->relationsIn = parent::relations([
                'REFERENCED_TABLE_NAME' => $this->baseTable,
            ]);
        }

        foreach ($this->relationsIn as $rel) {
            if ($rel['TABLE_NAME'] === $tableName) {
                return true;
            }
        }

        return false;

    }

}

