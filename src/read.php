<?php

namespace salby\cruddery;

use salby\dblyze\dblyze;

require_once('util.php');

class Read extends Dblyze
{

    protected $db;

    public function __construct($db)
    {
        $this->db = $db;
        parent::__construct($db);
    }

    public function exec($table, $config = [])
    {
        $defaults = [
            'filter' => [],
            'relations_out' => true,
            'relations_in' => true,
            'exclude' => [],
            'merge' => false
        ];
        $config = array_merge($defaults, $config);

        // Get columns from base table.
        $columnsBase = parent::columns($table);

        // Get outgoing relations if enabled.
        if ($config['relations_out'])
            $relOut = parent::relations([
                'TABLE_NAME' => $table
            ]);

        // Get incoming relations if enabled.
        if ($config['relations_in'])
            $relIn = parent::relations([
                'REFERENCED_TABLE_NAME' => $table
            ]);

        $select = [];
        $joins = [];

        // Outgoing relations and base columns.
        foreach ($columnsBase as $column) {
            if (!empty($relOut) && util::relatesOut($table, $column['Field'], $relOut)) {
                foreach ($relOut as $rel) {
                    if (
                        $rel['COLUMN_NAME'] === $column['Field']
                        &&
                        $rel['TABLE_NAME'] === $table
                        &&
                        $rel['REFERENCED_TABLE_NAME'] !== NULL
                        &&
                        !in_array($rel['REFERENCED_TABLE_NAME'], $config['exclude'])
                    ) {

                        // Find suitable column.
                        $col = parent::findColumn($rel['REFERENCED_TABLE_NAME']);

                        // Add select string.
                        $select[] =
                            "`".$rel['REFERENCED_TABLE_NAME'] . "`.`" . $col['Field']."`"
                            . " AS " .
                            $rel['TABLE_NAME'] . "_" . $rel['COLUMN_NAME'];

                        // Add join.
                        $joins[] = [
                            'table' => $rel['REFERENCED_TABLE_NAME'],
                            'column' => $rel['REFERENCED_COLUMN_NAME'],
                            'on' => util::formatSelect($table.'.'.$rel['COLUMN_NAME'])
                        ];

                    }
                }
            } else {

                // Add select string.
                $select[] =
                    "`".$table . "`.`" . $column['Field']."`"
                    . " AS " .
                    $table . "_" . $column['Field'];

            }
        }

        // Incoming relations.
        if ($config['relations_in'] && !empty($relIn)) {

            $concats = [];

            foreach ($relIn as $rel) {

                // Skip iteration if table is excluded.
                if (in_array($rel['TABLE_NAME'], $config['exclude']))
                    continue;

                // Add join.
                $joins[] = [
                    'table' => $rel['TABLE_NAME'],
                    'column' => $rel['COLUMN_NAME'],
                    'on' => util::formatSelect($rel['REFERENCED_TABLE_NAME'].'.'.$rel['REFERENCED_COLUMN_NAME'])
                ];

                // Get columns from table.
                $columns = parent::columns($rel['TABLE_NAME']);

                if (util::isRelationalTable($columns)) {

                    // Find outer table relation.
                    $relOut = parent::relations([
                        'TABLE_NAME' => $rel['TABLE_NAME']
                    ]);

                    foreach ($relOut as $outer) {
                        if (!in_array($outer['REFERENCED_TABLE_NAME'], [ NULL, $table])) {

                            // Add join.
                            $joins[] = [
                                'table' => $outer['REFERENCED_TABLE_NAME'],
                                'column' => $outer['REFERENCED_COLUMN_NAME'],
                                'on' => util::formatSelect($outer['TABLE_NAME'].'.'.$outer['COLUMN_NAME'])
                            ];

                            // Find suitable column.
                            $column = parent::findColumn($outer['REFERENCED_TABLE_NAME'], [
                                'exclude' => [
                                    'Key' => ['PRI', 'MUL'],
                                    'Comment' => 'ignore(read);'
                                ],
                            ]);

                            // Add select string.
                            $select[] =
                                "GROUP_CONCAT(`$outer[REFERENCED_TABLE_NAME]`.`$column[Field]` SEPARATOR '-----')"
                                . " AS " .
                                $table . "_" . $outer['REFERENCED_TABLE_NAME'];

                            // Add to list of concats.
                            $concats[] = $table . "_" . $outer['REFERENCED_TABLE_NAME'];

                        }
                    }

                } else {

                    // Find suitable column.
                    $column = parent::findColumn($columns, [
                        'exclude' => [
                            'Key' => ['PRI', 'MUL'],
                            'Comment' => 'ignore(read)'
                        ]
                    ]);

                    // Insert select string.
                    $select[] =
                        "GROUP_CONCAT(`$rel[TABLE_NAME]`.`$column[Field]` SEPARATOR '-----')"
                        . " AS " .
                        $rel['TABLE_NAME'] . "_" . $column['Field'];

                    // Add to list of concats.
                    $concats[] = $rel['TABLE_NAME'] . "_" . $column['Field'];

                }

            }

        }

        // Build join strings.
        $joinStrings = [];
        foreach ($joins as $join) {
            $joinStrings[] =
                "LEFT JOIN `$join[table]` ON $join[on] = `$join[table]`.`$join[column]`";
        }

        // Build parameters from $config['filter'].
        $params = [];
        $paramStrings = [];
        foreach ($config['filter'] as $column => $value) {

            // Set table if not specified.
            $column = substr_count($column, '.')
                ? util::formatSelect($select)
                : util::formatSelect($table . "." . $column);

            $prettyColumn = str_replace('.', '_', $column);
            $prettyColumn = str_replace('`', '', $column);

            if (is_array($value)) {

                $sanitizedValue = util::sanitize($value);
                $selectString = "$column IN (".implode(', ', $sanitizedValue).")";

            } else {

                $params[$prettyColumn] = $value;
                $selectString = "$column = :$prettyColumn";

            }

            $paramStrings[] = $selectString;

        }

        // Begin SQL query.
        $sql = "SELECT "
            .implode(', ', $select)
            ." FROM `$table`";

        // Add joins to SQL query.
        if (!empty($joinStrings))
            $sql .= " ".implode(' ', $joinStrings);

        // Add parameters to SQL query.
        if (!empty($paramStrings))
            $sql .= " WHERE ".implode(' AND ', $paramStrings);

        if (isset($config['group_by']) || isset($relIn)) {

            // Set column table if not set.
            if (isset($relIn))
                $column = util::formatSelect($table . "." . 'id');
            if (isset($config['group_by']))
                $column = substr_count($config['group_by'], '.')
                    ? util::formatSelect($config['group_by'])
                    : util::formatSelect($table . "." . $config['group_by']);

            // Add GROUP BY parameter to SQL query.
            $sql .= " GROUP BY $column";

        }

        if (isset($config['order_by'])) {

            // Specify table if not set.
            $column = substr_count($config['order_by'], '.')
                ? util::formatSelect($config['order_by'])
                : util::formatSelect($table . "." . $config['order_by']);

            // Add ORDER BY parameter to SQL query.
            $sql .= " ORDER BY $column";

        }

        if (isset($config['limit'])) {

            // Add LIMIT parameter to SQL query.
            $sql .= " LIMIT $config[limit]";

            // Add offset as well if defined in config.
            if (isset($config['offset']))
                $sql .= " OFFSET $config[offset]";

        }

        // Fetch rows.
        $rows = $this->db->fetch($sql, $params);

        // Merge concats.
        if ($config['relations_in'] && isset($concats) && !empty($concats)) {
            foreach ($rows as &$row) {
                foreach ($row as $column => $value) {

                    if (in_array($column, $concats)) {

                        // Save field name and unset item in array.
                        $field = $column;
                        unset($row[$column]);

                        // Get parsed values and insert them in new key.
                        $values = util::mergeConcat([ 'source' => $value ]);
                        $row[$field] = $values;

                    }

                }
            }
        }

        if ($config['merge'] && !empty($rows)) {
            return call_user_func_array('array_merge', $rows);
        } else {
            return $rows;
        }

    }

}
